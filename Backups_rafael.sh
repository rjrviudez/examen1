#Rafael Rodriguez 2018
#Curso programación estructurada IFCD0111
#Copia de los archivos del sistema

echo
echo "copia de los usuarios:"
echo "---------------------------------------------------------"
cp -r  home/  /backups/home
echo "---------------------------------------------------------"
echo "copiar las bases publicas de apache"
cp -r /xampp1/htdocs  /backups/htdocs
echo "---------------------------------------------------------"
echo "copiar la base de datos clientes  de mysql"
mysqldump -uroot -psecret -h192.168.105.85 -B clientes > /home/backups/clientes.sql
echo "---------------------------------------------------------"
echo "copiar la base de datos wordpress  de mysql"
mysqldump -uroot -psecret -h192.168.105.85 -B wordpress > /home/backups/wordpress.sql