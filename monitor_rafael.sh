#Rafael Rodriguez 2018
#Curso programación estructurada IFCD0111
#Monitorizador del  servidor

echo
echo "Información del servidor:"
echo "---------------------------------------------------------"
echo "Usuario:"
whoami
echo "---------------------------------------------------------"
echo "Día:"
date
echo "---------------------------------------------------------"
echo "Versión del SO:"
uname -a
echo "---------------------------------------------------------"
echo "Información de red:"
ifconfig
echo "Usuarios logeados:"
who -H
echo "Procesos del Sistema:"
ps aux
echo "---------------------------------------------------------"
echo "Ocupacion del disco , Memoria ram:"
echo
df -h
echo "---------------------------------------------------------"
free -h
echo "---------------------------------------------------------"
echo "Guardar informe en check_system :"
 echo "$(date)  $(uname -a)  $(ifconfig)  $(who -H)  $(ps aux) $(df -h) $(free -h) " > c:/xamp/htdocs/check_system.txt
